﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BingMapsRESTToolkit;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NLog;
using NLog.Config;
using NLog.Targets;
using PhysiciansMicroservice.Models;
using Location = PhysiciansMicroservice.Models.Location;

//using ProviderPcdbImport = PhysiciansMicroService.Models.ProviderPcdbImport;

namespace PhysiciansMicroService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class PhysiciansController : ControllerBase
    {
        private static IConfiguration _config;

        private readonly ProviderDataContext _dbContext;

        private static readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public PhysiciansController(IConfiguration c, ProviderDataContext dbContext)
        {
            _config = c;
            _dbContext = dbContext;
            var config = new LoggingConfiguration();
            var logFile = new FileTarget("logfile")
                { FileName = $"Logs/PhysiciansMicroservice-{DateTime.Today:MM-dd-yy}.log" };
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            LogManager.Configuration = config;
        }


        // GET: api/Physicians
            [HttpGet]
        public string Get()
        {
            const string message = "Physicians Micro Service Started....";
            _logger.Info(message);
            return message;
        }

        [HttpGet]
        [Route("GetHospitals")]
        public ActionResult GetHospitals()
        {
            try
            {
                var hospitalsList = new List<Hospital>();
                _logger.Info("Starting Getting Hospitals from db");
                try
                {
                    hospitalsList = _dbContext.Hospitals.ToList();
                    _logger.Info("List of Hospitals data retrieved from db");
                    return Ok(hospitalsList);
                }
                catch (Exception e)
                {
                    hospitalsList = null;
                    _logger.Info("Error retrieving Hospitals data from db");
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                _logger.Error("Exception Occurred in GetHospitals() " + e.Message + " " + e.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("GetGenders")]
        public ActionResult GetGenders()
        {
            try
            {
                _logger.Info("Starting Getting Genders from db");
                var providers = _dbContext.Providers.ToList();
                _logger.Info("List of Genders data retrieved from Provider db");
                var genders = providers.Where(x => x.GenderId != null).Select(x => x.GenderId).Distinct().ToList();
                return Ok(genders);
            }
            catch (Exception e)
            {
                _logger.Error("Exception Occurred in Genders() " + e.Message + " " + e.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("GetProviders")]
        public ActionResult GetProviders()
        {
            try
            {
                _logger.Info("Starting Getting Providers from db");
                _logger.Info("Database " + _dbContext.Database.GetConnectionString());

                var marketingProviders = new List<MarketingProvider>();

                var providers = _dbContext.Providers.ToList();
                foreach (var p in providers)
                {
                    var providerGuid = p.Guid;

                    //Provider
                    try
                    {
                        var mp = new MarketingProvider
                        {
                            Id = p.Id,
                            Guid = providerGuid,
                            FirstName = p.FirstName,
                            MiddleName = p.MiddleName,
                            LastName = p.LastName,
                            FormattedCommonName = p.FormattedCommonName,
                            FormattedLastNameFirst = p.FormattedLastNameFirst,
                            EchoPhysicianId = p.EchoPhysicianId,
                            EchoDoctorNumber = p.EchoDoctorNumber,
                            NationalId = p.NationalId,
                            ContactPhone = p.ContactPhone,
                            ContactCellPhone = p.ContactCellPhone,
                            ContactPager = p.ContactPager,
                            GenderId = p.GenderId,
                            DoctorImage = p.DoctorImage,
                            DoctorHeaderImage = p.DoctorHeaderImage,
                            ThumbnailPhoto = p.ThumbnailPhoto,
                            EchoSuffix = p.EchoSuffix,
                            UrlRoute = p.UrlRoute,
                            FormattedUrlRoute = p.FormattedUrlRoute,
                            IsAcceptingNewPatients = (bool)p.IsAcceptingNewPatients,
                            IsAcceptingNewPatientsVisible = (bool)p.IsAcceptingNewPatientsVisible,
                            TypeId = (int)p.TypeId,
                            Introduction = p.Introduction,
                            FormattedSpecialties = p.FormattedSpecialties,
                            IsArchived = (bool)p.IsArchived,
                            IsHide = (bool)p.IsHide,
                            NameOverride = p.NameOverride,
                            JobTitleOverride = p.JobTitleOverride,
                            FormattedJobTitle = p.FormattedJobTitle,
                            LegalPracticeName = p.LegalPracticeName,
                            DoesAcceptsEapps = (bool)p.DoesAcceptsEapps,
                            IsPCMH = (bool)p.IsPcmh,
                            IsBloodless = (bool)p.IsBloodless,
                            AdminApproved = (bool)p.AdminApproved,
                            IsNewDoctor = (bool)p.IsNewDoctor,
                            IsBPP = (bool)p.IsBpp,
                            IsEmployed = p.IsEmployed,
                            DoesHavePatientReviews = (bool)p.DoesHavePatientReviews,
                            PublicationQuery = p.PublicationQuery,
                            LastUpdateDateTime = p.LastUpdateDateTime,
                            LastUpdateUser = p.LastUpdateUser,
                        };

                        //Type
                        try
                        {
                            var types = _dbContext.Types.FirstOrDefault(x => x.ProviderGuid == providerGuid);
                            mp.Type = new _Type
                            {
                                Id = (int)types.Id,
                                Code = types.Code,
                                Text = types.Text
                            };
                        }
                        catch (Exception e)
                        {
                            _logger.Error("There was an error retrieving Provider Type data from db " + p.FirstName + " " + p.LastName + " :: Exception: " + e.Message);
                        }

                        //Groups Mapping
                        try
                        {
                            var groups = _dbContext.GroupsMappings.Where(x => x.ProviderGuid == providerGuid).ToList();

                            mp.GroupsMapping = new List<_GroupsMapping>();
                            foreach (var groupMappingg in groups)
                            {
                                var g = new _GroupsMapping
                                {
                                    Code = groupMappingg.Code,
                                    Text = groupMappingg.Text,
                                    Type = groupMappingg.Type,
                                    OrderId = (int)groupMappingg.OrderId,
                                    IsPrimary = (bool)groupMappingg.IsPrimary
                                };
                                mp.GroupsMapping.Add(g);
                            }
                        }
                        catch (Exception e)
                        {
                            _logger.Error("There was an error retrieving Provider Group Mappings data from db " + p.FirstName + " " + p.LastName + " :: Exception: " + e.Message);
                        }

                        //Specialty Mapping
                        try
                        {
                            var specialties = _dbContext.SpecialtyMappings.Where(x => x.ProviderGuid == providerGuid).ToList();
                            mp.SpecialtyMapping = new List<_SpecialtyMapping>();
                            foreach (var specialtyMapping in specialties)
                            {
                                var s = new _SpecialtyMapping()
                                {
                                    SpecialtyCode = specialtyMapping.SpecialtyCode,
                                    Text = specialtyMapping.Text,
                                    JobTitle = specialtyMapping.JobTitle,
                                    SpecialtyStatus = specialtyMapping.SpecialtyStatus,
                                    SpecialtyType = specialtyMapping.SpecialtyType,
                                    HippaTaxonomy = specialtyMapping.HippaTaxonomy,
                                    IsBoardCertified = (bool)specialtyMapping.IsBoardCertified,
                                    IsPrimary = (bool)specialtyMapping.IsPrimary
                                };
                                mp.SpecialtyMapping.Add(s);
                            }
                        }
                        catch (Exception e)
                        {
                            _logger.Error("There was an error retrieving Provider Specialty Mappings data from db " + p.FirstName + " " + p.LastName + " :: Exception: " + e.Message);
                        }

                        //Language Mapping
                        try
                        {
                            var languages = _dbContext.LanguageMappings.Where(x => x.ProviderGuid == providerGuid).ToList();
                            mp.LanguageMapping = new List<_LanguageMapping>();
                            foreach (var languageMapping in languages)
                            {
                                var l = new _LanguageMapping
                                {
                                    LanguageCode = languageMapping.LanguageCode,
                                    Text = languageMapping.Text
                                };
                                mp.LanguageMapping.Add(l);
                            }
                        }
                        catch (Exception e)
                        {
                            _logger.Error("There was an error retrieving Provider Language Mappings data from db " + p.FirstName + " " + p.LastName + " :: Exception: " + e.Message);
                        }

                        //Education Mapping
                        try
                        {
                            var educationList = _dbContext.EducationMappings.Where(x => x.ProviderGuid == providerGuid).ToList();
                            mp.EducationMapping = new List<_EducationMapping>();
                            foreach (var educationMapping in educationList)
                            {
                                var e = new _EducationMapping
                                {
                                    DegreeCode = educationMapping.DegreeCode,
                                    DegreeText = educationMapping.DegreeText,
                                    ProgramCode = educationMapping.ProgramCode,
                                    ProgramText = educationMapping.ProgramText,
                                    InsitutionCode = educationMapping.InsitutionCode,
                                    InstitutionText = educationMapping.InstitutionText,
                                    InstitutionCity = educationMapping.InstitutionCity,
                                    InstitutionState = educationMapping.InstitutionState,
                                    Graduate_Complete = educationMapping.GraduateComplete,
                                    StartYear = educationMapping.StartYear,
                                    EndYear = educationMapping.EndYear,
                                    SequenceId = educationMapping.SequenceId
                                };
                                mp.EducationMapping.Add(e);
                            }
                        }
                        catch (Exception e)
                        {
                            _logger.Error("There was an error retrieving Provider Education Mappings data from db " + p.FirstName + " " + p.LastName + " :: Exception: " + e.Message);
                        }

                        //Suffix Mapping
                        try
                        {
                            var suffixes = _dbContext.SuffixMappings.Where(x => x.ProviderGuid == providerGuid).ToList();
                            mp.SuffixMapping = new List<_SuffixMapping>();
                            foreach (var suffixMapping in suffixes)
                            {
                                var s = new _SuffixMapping
                                {
                                    SuffixCode = suffixMapping.SuffixCode,
                                    OrderId = (int)suffixMapping.OrderId
                                };
                                mp.SuffixMapping.Add(s);
                            }
                        }
                        catch (Exception e)
                        {
                            _logger.Error("There was an error retrieving Provider Suffix Mappings data from db " + p.FirstName + " " + p.LastName + " :: Exception: " + e.Message);
                        }

                        //Hospital Mapping
                        try
                        {
                            var hospitals = _dbContext.HospitalMappings.Where(x => x.ProviderGuid == providerGuid).ToList();
                            mp.HospitalMapping = new List<_HospitalMapping>();
                            foreach (var hospitalMapping in hospitals)
                            {
                                var h = new _HospitalMapping
                                {
                                    HospitalCode = hospitalMapping.HospitalCode,
                                    HospitalName = hospitalMapping.HospitalName,
                                    StaffCode = hospitalMapping.StaffCode,
                                    OrderId = hospitalMapping.OrderId
                                };
                                mp.HospitalMapping.Add(h);
                            }
                        }
                        catch (Exception e)
                        {
                            _logger.Error("There was an error retrieving Provider Hospital Mappings data from db " + p.FirstName + " " + p.LastName + " :: Exception: " + e.Message);
                        }

                        //Ages Treated Mappings
                        try
                        {
                            var ages = _dbContext.AgesTreatedMappings.Where(x => x.ProviderGuid == providerGuid).ToList();
                            mp.AgesTreatedMappings = new List<_AgesTreatedMapping>();
                            foreach (var agesTreatedMapping in ages)
                            {
                                var a = new _AgesTreatedMapping
                                {
                                    Text = agesTreatedMapping.Text,
                                    OrderId = (int)agesTreatedMapping.OrderId
                                };
                                mp.AgesTreatedMappings.Add(a);
                            }
                        }
                        catch (Exception e)
                        {
                            _logger.Error("There was an error retrieving Provider Ages Treated Mappings data from db " + p.FirstName + " " + p.LastName + " :: Exception: " + e.Message);
                        }

                        //Patient Forms Mapping
                        try
                        {
                            var patientForms = _dbContext.PatientFormsMappings.Where(x => x.ProviderGuid == providerGuid).ToList();
                            mp.PatientFormsMappings = new List<_PatientFormsMapping>();
                            foreach (var patientFormsMapping in patientForms)
                            {
                                var pF = new _PatientFormsMapping
                                {
                                    Name = patientFormsMapping.Name,
                                    Type = patientFormsMapping.Type,
                                    Link = patientFormsMapping.Link,
                                    Description = patientFormsMapping.Description,
                                    OrderId = (int)patientFormsMapping.OrderId
                                };
                                mp.PatientFormsMappings.Add(pF);
                            }
                        }
                        catch (Exception e)
                        {
                            _logger.Error("There was an error retrieving Provider Patient Forms Mappings data from db " + p.FirstName + " " + p.LastName + " :: Exception: " + e.Message);
                        }

                        //Location Mapping
                        try
                        {
                            // get locationMapping from DB for Provider
                            var locationMapping = _dbContext.LocationMappings.Where(x => x.ProviderGuid == providerGuid)
                                .ToList();

                            var locations = locationMapping.Select(map => _dbContext.Locations.FirstOrDefault(x => x.LocationGuid == map.LocationGuid)).ToList();
                            //var locations = _dbContext.Locations.Where(location =>
                            //        _dbContext.LocationMappings.Any(x =>
                            //            x.LocationGuid == location.LocationGuid && x.ProviderGuid == providerGuid))
                            //    .ToList();
                            mp.Locations = new List<Locations>();
                            foreach (var location in locations)
                            {
                                // get location type
                                //var locationMapping = _dbContext.LocationMappings.FirstOrDefault(x => x.LocationGuid == location.LocationGuid);
                                var l = new Locations
                                {
                                    LocationId = (int)location.LocationId,
                                    LocationGuid = location.LocationGuid,
                                    Name = location.Name,
                                    Address1 = location.Address1,
                                    Address2 = location.Address2,
                                    Address3 = location.Address3,
                                    City = location.City,
                                    State = location.State,
                                    Zip = location.Zip,
                                    Phone = location.Phone,
                                    Fax = location.Fax,
                                    IsAcceptingEAppointments = location.IsAcceptingEappointments,
                                    IsDefaultLocation = locationMapping.Where(x=>x.LocationGuid==location.LocationGuid).Select(x=>x.IsDefaultLocation).FirstOrDefault(),//locationMapping.IsDefaultLocation,
                                    SequenceId = (int)location.SequenceId,
                                    Lat = (double)location.Lat,
                                    Lng = (double)location.Lng,
                                    type = locationMapping.Where(x=>x.LocationGuid==location.LocationGuid).Select(x=>x.Type).FirstOrDefault(),//locationMapping.Type,
                                    mainPhone = location.MainPhone,
                                    admissionsPhone = location.AdmissionsPhone
                                };
                                     //Location Hours
                                 try
                                 {
                                     var locationHours = _dbContext.LocationsHours.Where(x => x.LocationGuid==location.LocationGuid).ToList();
                                     l.Hours = new List<Locations._Hours>();
                                     if (locationHours != null || locationHours.Count > 0)
                                     {
                                         foreach (var lh in locationHours.Select(locationHr => new Locations._Hours
                                         {
                                             DayOfWeek = locationHr.DayOfWeek,
                                             OpenTime = locationHr.OpenTime,
                                             CloseTime = locationHr.CloseTime
                                         }))
                                         {
                                             l.Hours.Add(lh);
                                         }
                                     }
                                 }
                                 catch (Exception e)
                                 {
                                     _logger.Error("There was an error retrieving Provider Locations Hours Mappings data from db " + p.FirstName + " " + p.LastName + " :: Exception: " + e.Message);
                                 }

                                 //Other Providers
                                 try
                                 {
                                     var otherProviders = _dbContext.LocationsProviders.Where(x => x.LocationGuid==location.LocationGuid).ToList();
                                     l.OtherProviders = new List<Locations._Providers>();
                                     if (otherProviders != null || otherProviders.Count > 0)
                                     {
                                         foreach (var op in otherProviders.Select(locationsProvider => new Locations._Providers
                                         {
                                             Guid = locationsProvider.ProviderGuid,
                                             CommonName = locationsProvider.CommonName,
                                             UrlRoute = locationsProvider.UrlRoute,
                                             JobTitle = locationsProvider.JobTitle
                                         }))
                                         {
                                             l.OtherProviders.Add(op);
                                         }
                                     }
                                 }
                                 catch (Exception e)
                                 {
                                     _logger.Error("There was an error retrieving Provider Locations Other Providers Mappings data from db " + p.FirstName + " " + p.LastName + " :: Exception: " + e.Message);
                                 }

                                 //Office Extenders
                                 try
                                 {
                                     var officeExtenders = _dbContext.LocationsOfficeExtenders.Where(x => x.LocationGuid==location.LocationGuid).ToList();
                                     l.OfficeExtenders = new List<Locations._Providers>();
                                     if (officeExtenders != null || officeExtenders.Count > 0)
                                     {
                                         foreach (var oe in officeExtenders.Select(locationsOfficeExtender => new Locations._Providers
                                         {
                                             Guid = locationsOfficeExtender.ExtenderGuid,
                                             CommonName = locationsOfficeExtender.CommonName,
                                             UrlRoute = locationsOfficeExtender.UrlRoute,
                                             JobTitle = locationsOfficeExtender.JobTitle
                                         }))
                                         {
                                             l.OfficeExtenders.Add(oe);
                                         }
                                     }

                                     mp.Locations.Add(l);
                                 }
                                 catch (Exception e)
                                 {
                                     _logger.Error("There was an error retrieving Provider Locations Office Extenders Mappings data from db " + p.FirstName + " " + p.LastName + " :: Exception: " + e.Message);
                                 }
                            }
                           
                        }
                        catch (Exception e)
                        {
                            _logger.Error("There was an error retrieving Provider Locations Mappings data from db " + p.FirstName + " " + p.LastName + " :: Exception: " + e.Message);
                        }

                        //Board Mapping
                        try
                        {
                            var boardsList = _dbContext.BoardMappings.Where(x => x.ProviderGuid == providerGuid).ToList();
                            mp.BoardMapping = new List<_BoardMapping>();
                            foreach (var b in boardsList.Select(boardMapping => new _BoardMapping
                            {
                                BoardCode = boardMapping.BoardCode,
                                Text = boardMapping.Text,
                                BoardStatus = boardMapping.BoardStatus,
                                CertificationDate = boardMapping.CertificationDate,
                                RecertificationDate = boardMapping.RecertificationDate,
                                ExpirationDate = boardMapping.ExpirationDate,
                                CertificationNumber = boardMapping.CertificationNumber,
                                UserDef_M1 = boardMapping.UserDefM1,
                                Active = (bool)boardMapping.Active
                            }))
                            {
                                mp.BoardMapping.Add(b);
                            }
                        }
                        catch (Exception e)
                        {
                            _logger.Error("There was an error retrieving Provider Board Mappings data from db " + p.FirstName + " " + p.LastName + " :: Exception: " + e.Message);
                        }

                        //License Mapping
                        try
                        {
                            var licenses = _dbContext.LicenseMappings.Where(x => x.ProviderGuid == providerGuid).ToList();
                            mp.LicenseMapping = new List<_LicenseMapping>();
                            foreach (var l in licenses.Select(licenseMapping => new _LicenseMapping
                            {
                                LicenseType = licenseMapping.LicenseType,
                                LicenseStatus = licenseMapping.LicenseStatus,
                                State = licenseMapping.State,
                                LicenseNumber = licenseMapping.LicenseNumber,
                                AwardDate = licenseMapping.AwardDate,
                                ExpirationDate = licenseMapping.ExpirationDate,
                                Contact_Name = licenseMapping.ContactName,
                                COntact_Fax = licenseMapping.ContactFax,
                                Contact_Email = licenseMapping.ContactEmail,
                                LicensureField = licenseMapping.LicensureField,
                                Institution_Name = licenseMapping.InstitutionName,
                                Institution_Contact = licenseMapping.InstitutionContact,
                                Institution_Address1 = licenseMapping.InstitutionAddress1,
                                Institution_Address2 = licenseMapping.InstitutionAddress2,
                                Institution_City = licenseMapping.InstitutionCity,
                                Institution_State = licenseMapping.InstitutionState,
                                Institution_Zip = licenseMapping.InstitutionZip,
                                Active = (bool)licenseMapping.Active
                            }))
                            {
                                mp.LicenseMapping.Add(l);
                            }
                        }
                        catch (Exception e)
                        {
                            _logger.Error("There was an error retrieving Provider License Mappings data from db " + p.FirstName + " " + p.LastName + " :: Exception: " + e.Message);
                        }

                        //Expertise
                        try
                        {
                            var expertises = _dbContext.Expertises.Where(x => x.ProviderGuid == providerGuid).ToList();
                            mp.Expertise = new List<_Expertise>();
                            foreach (var e in expertises.Select(expertise => new _Expertise
                            {
                                Text = expertise.Text,
                                OrderId = expertise.OrderId
                            }))
                            {
                                mp.Expertise.Add(e);
                            }
                        }
                        catch (Exception e)
                        {
                            _logger.Error("There was an error retrieving Provider Expertise data from db " + p.FirstName + " " + p.LastName + " :: Exception: " + e.Message);
                        }

                        //Provider Paragraphs
                        try
                        {
                            var providerParagraphs = _dbContext.ProviderParagraphs.Where(x => x.ProviderGuid == providerGuid).ToList();
                            mp.ProviderParagraphs = new List<_ProviderParagraphs>();
                            foreach (var pp in providerParagraphs.Select(providerParagraph => new _ProviderParagraphs
                            {
                                Text = providerParagraph.Text,
                                OrderId = providerParagraph.OrderId
                            }))
                            {
                                mp.ProviderParagraphs.Add(pp);
                            }
                        }
                        catch (Exception e)
                        {
                            _logger.Error("There was an error retrieving Provider Paragraphs data from db " + p.FirstName + " " + p.LastName + " :: Exception: " + e.Message);
                        }

                        marketingProviders.Add(mp);
                    }
                    catch (Exception e)
                    {
                        _logger.Error("There was an error retrieving Provider from db " + p.FirstName + " " + p.LastName);
                    }
                }

                return Ok(marketingProviders);
            }
            catch (Exception e)
            {
                _logger.Error("Exception Occurred in GetProviders() " + e.Message + " " + e.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("GetLanguages")]
        public ActionResult GetLanguages()
        {
            _logger.Info("Started Getting Languages ... ");
            try
            {
                var languagesList = new List<Language>();
                _logger.Info("Starting Getting Languages from db");
                try
                {
                    languagesList = _dbContext.Languages.ToList();
                    _logger.Info("List of Languages data retrieved from db");
                    return Ok(languagesList);
                }
                catch (Exception e)
                {
                    languagesList = null;
                    _logger.Info("Error retrieving Languages data from db");
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                _logger.Error("Exception Occurred in GetLanguages() " + e.Message + " " + e.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("GetSpecialties")]
        public ActionResult GetSpecialties()
        {
            _logger.Info("Started Getting Specialties ... ");
            try
            {
                var specialtyList = new List<Specialty>();
                _logger.Info("Starting Getting Specialties from db");
                try
                {
                    specialtyList = _dbContext.Specialties.ToList();
                    _logger.Info("List of Specialties data retrieved from db");
                    return Ok(specialtyList);
                }
                catch (Exception e)
                {
                    specialtyList = null;
                    _logger.Info("Error retrieving Specialties data from db");
                    return BadRequest();
                }
            }
            catch (Exception e)
            {
                _logger.Error("Exception Occurred in GetSpecialties() " + e.Message + " " + e.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("PcdbImport")]
        public ActionResult PcdbImport()
        {
            var pcdbImportProviders = new List<ProviderPcdbImport>();

            try
            {
                _logger.Info("Starting Getting Providers from Service for pcdb Import");

                var providers = _dbContext.Providers.ToList();

                foreach (var p in providers)
                {
                    try
                    {
                        var pcdb = new ProviderPcdbImport
                        {
                            FirstName = p.FirstName, LastName = p.LastName, EchoDoctorNumber = p.EchoDoctorNumber
                        };
                        pcdbImportProviders.Add(pcdb);
                    }
                    catch (Exception e)
                    {
                        _logger.Error("Exception occurred mapping Provider Info for Provider DoctorNumber: " + p.EchoDoctorNumber + " Provider Name: " +
                                         p.FirstName + " " + p.LastName);
                        return BadRequest();
                    }
                }

                return Ok(pcdbImportProviders);
            }
            catch (Exception e)
            {
                _logger.Error("Exception Occurred in GetProvidersForPcdbImport() " + e.Message + " " + e.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("GetIntranetFAPProviders")]
        public ActionResult GetIntranetFAPProviders()
        {
            var providerList = new List<FinalFAPProvider>();
            try
            {
                var providers = _dbContext.FapprovidersViews.ToList();

                foreach (var p in providers)
                {
                    var provider = new FinalFAPProvider
                    {
                        _Fapproviders = new FapprovidersView
                        {
                            Id = p.Id,
                            Guid = p.Guid,
                            FirstName = p.FirstName,
                            MiddleName = p.MiddleName,
                            LastName = p.LastName,
                            DoctorImage = p.DoctorImage,
                            FormattedJobTitle = p.FormattedJobTitle,
                            FormattedCommonName = p.FormattedCommonName,
                            EchoPhysicianId = p.EchoPhysicianId,
                            GenderId = p.GenderId,
                            EchoSuffix = p.EchoSuffix,
                            IsBpp = p.IsBpp,
                            IsAcceptingNewPatients = p.IsAcceptingNewPatients,
                            LegalPracticeName = p.LegalPracticeName,
                            HospitalCodes = p.HospitalCodes,
                            SpecialtyCodes = p.SpecialtyCodes,
                            LanguageCodes = p.LanguageCodes,
                            OtherAddressIds = p.OtherAddressIds,
                            AgesTreatedText = p.AgesTreatedText
                        }
                    };
                    // get primary address
                    var primaryMapping =
                        _dbContext.LocationMappings.FirstOrDefault(x => x.Type == "Primary Address" && x.ProviderGuid == p.Guid);
                    var primaryAddress = new Location();
                    if (primaryMapping != null)
                    {
                        primaryAddress =
                            _dbContext.Locations.FirstOrDefault(x => x.LocationGuid == primaryMapping.LocationGuid);
                    }
                    //var primaryAddress =_dbContext.Locations.FirstOrDefault(x => x.Type == "Primary Address" && x.ProviderGuid == p.Guid);

                    if (primaryAddress == null)
                    {
                        var defaultMapping =
                            _dbContext.LocationMappings.FirstOrDefault(x => x.IsDefaultLocation && x.ProviderGuid == p.Guid);
                        var defaultLocation = new Location();
                        if (defaultMapping != null)
                        {
                            defaultLocation = _dbContext.Locations.FirstOrDefault(x => x.LocationGuid == defaultMapping.LocationGuid);
                        }

                        //var defaultLocation = _dbContext.Locations.FirstOrDefault(x => x.IsDefaultLocation && x.ProviderGuid == p.Guid);
                        if (defaultLocation == null)
                        {
                            var notPrimary = _dbContext.LocationMappings.FirstOrDefault(x =>
                                x.Type != "Primary Address" && x.ProviderGuid == p.Guid);
                            var otherAddress =
                                _dbContext.Locations.FirstOrDefault(x => x.LocationGuid == notPrimary.LocationGuid);
                            //var otherAddress = _dbContext.Locations.FirstOrDefault(x => x.Type != "Primary Address" && x.ProviderGuid == p.Guid);
                            if (otherAddress != null)
                            {
                                provider.Address1 = otherAddress.Address1;
                                provider.Address2 = otherAddress.Address2;
                                provider.Address3 = otherAddress.Address3;
                                provider.City = otherAddress.City;
                                provider.State = otherAddress.State;
                                provider.Zip = otherAddress.Zip;
                                provider.Fax = otherAddress.Fax;
                                provider.Lat = otherAddress.Lat;
                                provider.Lng = otherAddress.Lng;
                                provider.MainPhone = otherAddress.MainPhone;
                            }
                        }
                        else
                        {
                            provider.Address1 = defaultLocation.Address1;
                            provider.Address2 = defaultLocation.Address2;
                            provider.Address3 = defaultLocation.Address3;
                            provider.City = defaultLocation.City;
                            provider.State = defaultLocation.State;
                            provider.Zip = defaultLocation.Zip;
                            provider.Fax = defaultLocation.Fax;
                            provider.Lat = defaultLocation.Lat;
                            provider.Lng = defaultLocation.Lng;
                            provider.MainPhone = defaultLocation.MainPhone;
                        }
                    }
                    else
                    {
                        provider.Address1 = primaryAddress.Address1;
                        provider.Address2 = primaryAddress.Address2;
                        provider.Address3 = primaryAddress.Address3;
                        provider.City = primaryAddress.City;
                        provider.State = primaryAddress.State;
                        provider.Zip = primaryAddress.Zip;
                        provider.Fax = primaryAddress.Fax;
                        provider.Lat = primaryAddress.Lat;
                        provider.Lng = primaryAddress.Lng;
                        provider.MainPhone = primaryAddress.MainPhone;
                    }

                    providerList.Add(provider);
                }

                return Ok(providerList);
            }
            catch (Exception e)
            {
                providerList = null;
                _logger.Error("Exception Occurred in GetIntranetFAPProviders() " + e.Message + " " + e.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("GetIntranetHospitals")]
        public ActionResult GetIntranetHospitals()
        {
            try
            {
                var hospitals = _dbContext.Hospitals.ToList();

                return Ok(hospitals);
            }
            catch (Exception e)
            {
                _logger.Error("Exception Occurred in GetIntranetHospitals() " + e.Message + " " + e.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("GetIntranetSpecialties")]
        public ActionResult GetIntranetSpecialties()
        {
            try
            {
                var specialties = _dbContext.Specialties.ToList();

                return Ok(specialties);
            }
            catch (Exception e)
            {
                _logger.Error("Exception Occurred in GetIntranetSpecialties() " + e.Message + " " + e.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("GetIntranetLanguages")]
        public ActionResult GetIntranetLanguages()
        {
            try
            {
                var languages = _dbContext.Languages.ToList();

                return Ok(languages);
            }
            catch (Exception e)
            {
                _logger.Error("Exception Occurred in GetIntranetLanguages() " + e.Message + " " + e.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("GetIntranetLocations")]
        public ActionResult GetIntranetLocations()
        {
            try
            {
                var locations = _dbContext.Locations.ToList();

                return Ok(locations);
            }
            catch (Exception e)
            {
                _logger.Error("Exception Occurred in GetIntranetLocations() " + e.Message + " " + e.StackTrace);
                return BadRequest();
            }
        }

        [HttpGet]
        [Route("GetIntranetLocationZip")]
        public ActionResult GetIntranetLocationZip()
        {
            var headers = Request.Headers;
            try
            {
                var zip = "";

                if (headers.ContainsKey("zip"))
                {
                    zip = headers["zip"].First();
                }
            
                var locations = _dbContext.Locations.FirstOrDefault(x => x.Zip.Contains(zip));

                return Ok(locations);
            }
            catch (Exception e)
            {
                _logger.Error("Exception Occurred in GetIntranetLocationZip() " + e.Message + " " + e.StackTrace);
                return BadRequest();
            }
        }
    }
}
