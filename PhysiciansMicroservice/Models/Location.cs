﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PhysiciansMicroservice.Models
{
    public partial class Location
    {
        public Location()
        {
            LocationMappings = new HashSet<LocationMapping>();
            LocationsHours = new HashSet<LocationsHour>();
            LocationsOfficeExtenders = new HashSet<LocationsOfficeExtender>();
            LocationsProviders = new HashSet<LocationsProvider>();
        }

        public int Id { get; set; }
        public int? LocationId { get; set; }
        public string LocationGuid { get; set; }
        public string Name { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public bool IsAcceptingEappointments { get; set; }
        public int? SequenceId { get; set; }
        public double? Lat { get; set; }
        public double? Lng { get; set; }
        public string MainPhone { get; set; }
        public string AdmissionsPhone { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime InsertDate { get; set; }
        public string RecentUser { get; set; }

        public virtual ICollection<LocationMapping> LocationMappings { get; set; }
        public virtual ICollection<LocationsHour> LocationsHours { get; set; }
        public virtual ICollection<LocationsOfficeExtender> LocationsOfficeExtenders { get; set; }
        public virtual ICollection<LocationsProvider> LocationsProviders { get; set; }
    }
}
