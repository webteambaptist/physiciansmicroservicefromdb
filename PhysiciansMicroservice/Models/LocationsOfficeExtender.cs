﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PhysiciansMicroservice.Models
{
    public partial class LocationsOfficeExtender
    {
        public int Id { get; set; }
        public string LocationGuid { get; set; }
        public string ExtenderGuid { get; set; }
        public string CommonName { get; set; }
        public string UrlRoute { get; set; }
        public string JobTitle { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime InsertDate { get; set; }
        public string RecentUser { get; set; }

        public virtual Location LocationGu { get; set; }
    }
}
