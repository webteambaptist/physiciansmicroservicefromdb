﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PhysiciansMicroservice.Models
{
    public partial class EducationMapping
    {
        public int Id { get; set; }
        public string ProviderGuid { get; set; }
        public string DegreeCode { get; set; }
        public string DegreeText { get; set; }
        public string ProgramCode { get; set; }
        public string ProgramText { get; set; }
        public string InsitutionCode { get; set; }
        public string InstitutionText { get; set; }
        public string InstitutionCity { get; set; }
        public string InstitutionState { get; set; }
        public string GraduateComplete { get; set; }
        public string StartYear { get; set; }
        public string EndYear { get; set; }
        public string SequenceId { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime InsertDate { get; set; }
        public string RecentUser { get; set; }

        public virtual Provider ProviderGu { get; set; }
    }
}
