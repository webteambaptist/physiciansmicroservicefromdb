﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PhysiciansMicroservice.Models
{
    public partial class LocationMapping
    {
        public int Id { get; set; }
        public string LocationGuid { get; set; }
        public string ProviderGuid { get; set; }
        public bool IsDefaultLocation { get; set; }
        public string Type { get; set; }

        public virtual Location LocationGu { get; set; }
        public virtual Provider ProviderGu { get; set; }
    }
}
