﻿using System;
using System.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace PhysiciansMicroservice.Models
{
    public partial class ProviderDataContext : DbContext
    {
        public ProviderDataContext()
        {
        }

        public ProviderDataContext(DbContextOptions<ProviderDataContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AgesTreatedMapping> AgesTreatedMappings { get; set; }
        public virtual DbSet<BoardMapping> BoardMappings { get; set; }
        public virtual DbSet<EducationMapping> EducationMappings { get; set; }
        public virtual DbSet<Expertise> Expertises { get; set; }
        public virtual DbSet<FapprovidersView> FapprovidersViews { get; set; }
        public virtual DbSet<GroupsMapping> GroupsMappings { get; set; }
        public virtual DbSet<Hospital> Hospitals { get; set; }
        public virtual DbSet<HospitalMapping> HospitalMappings { get; set; }
        public virtual DbSet<Language> Languages { get; set; }
        public virtual DbSet<LanguageMapping> LanguageMappings { get; set; }
        public virtual DbSet<LicenseMapping> LicenseMappings { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<LocationMapping> LocationMappings { get; set; }
        public virtual DbSet<LocationsHour> LocationsHours { get; set; }
        public virtual DbSet<LocationsOfficeExtender> LocationsOfficeExtenders { get; set; }
        public virtual DbSet<LocationsProvider> LocationsProviders { get; set; }
        public virtual DbSet<PatientFormsMapping> PatientFormsMappings { get; set; }
        public virtual DbSet<Provider> Providers { get; set; }
        public virtual DbSet<ProviderParagraph> ProviderParagraphs { get; set; }
        public virtual DbSet<Specialty> Specialties { get; set; }
        public virtual DbSet<SpecialtyMapping> SpecialtyMappings { get; set; }
        public virtual DbSet<SuffixMapping> SuffixMappings { get; set; }
        public virtual DbSet<Type> Types { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "SQL_Latin1_General_CP1_CI_AS");

            modelBuilder.Entity<AgesTreatedMapping>(entity =>
            {
                entity.ToTable("AgesTreatedMapping");

                entity.Property(e => e.InsertDate).HasColumnType("datetime");

                entity.Property(e => e.ProviderGuid)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RecentUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Text)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.HasOne(d => d.ProviderGu)
                    .WithMany(p => p.AgesTreatedMappings)
                    .HasForeignKey(d => d.ProviderGuid)
                    .HasConstraintName("FK_AgesTreatedMapping_Provider");
            });

            modelBuilder.Entity<BoardMapping>(entity =>
            {
                entity.ToTable("BoardMapping");

                entity.Property(e => e.BoardCode)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.BoardStatus)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CertificationDate)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.CertificationNumber)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ExpirationDate)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InsertDate).HasColumnType("datetime");

                entity.Property(e => e.ProviderGuid)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RecentUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.RecertificationDate)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Text)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UserDefM1)
                    .HasMaxLength(255)
                    .IsUnicode(false)
                    .HasColumnName("UserDef_M1");

                entity.HasOne(d => d.ProviderGu)
                    .WithMany(p => p.BoardMappings)
                    .HasForeignKey(d => d.ProviderGuid)
                    .HasConstraintName("FK_BoardMapping_Provider");
            });

            modelBuilder.Entity<EducationMapping>(entity =>
            {
                entity.ToTable("EducationMapping");

                entity.Property(e => e.DegreeCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DegreeText)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.EndYear)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.GraduateComplete)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("Graduate_Complete");

                entity.Property(e => e.InsertDate).HasColumnType("datetime");

                entity.Property(e => e.InsitutionCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InstitutionCity)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InstitutionState)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InstitutionText)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProgramCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProgramText)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProviderGuid)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RecentUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SequenceId)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.StartYear)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.HasOne(d => d.ProviderGu)
                    .WithMany(p => p.EducationMappings)
                    .HasForeignKey(d => d.ProviderGuid)
                    .HasConstraintName("FK_EducationMapping_Provider");
            });

            modelBuilder.Entity<Expertise>(entity =>
            {
                entity.ToTable("Expertise");

                entity.Property(e => e.InsertDate).HasColumnType("datetime");

                entity.Property(e => e.OrderId)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProviderGuid)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RecentUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Text).IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.HasOne(d => d.ProviderGu)
                    .WithMany(p => p.Expertises)
                    .HasForeignKey(d => d.ProviderGuid)
                    .HasConstraintName("FK_Expertise_Provider");
            });

            modelBuilder.Entity<FapprovidersView>(entity =>
            {
                entity.HasNoKey();

                entity.ToView("FAPProviders_View");

                entity.Property(e => e.DoctorImage)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.EchoDoctorNumber)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EchoPhysicianId)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EchoSuffix)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FormattedCommonName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FormattedJobTitle)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.GenderId)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Guid)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.IsBpp).HasColumnName("IsBPP");

                entity.Property(e => e.LastName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LegalPracticeName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MiddleName)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<GroupsMapping>(entity =>
            {
                entity.ToTable("GroupsMapping");

                entity.Property(e => e.Code)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InsertDate).HasColumnType("datetime");

                entity.Property(e => e.ProviderGuid)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RecentUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Text)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.HasOne(d => d.ProviderGu)
                    .WithMany(p => p.GroupsMappings)
                    .HasForeignKey(d => d.ProviderGuid)
                    .HasConstraintName("FK_GroupsMapping_Provider");
            });

            modelBuilder.Entity<Hospital>(entity =>
            {
                entity.Property(e => e.HospitalCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.HospitalName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InsertDate).HasColumnType("datetime");

                entity.Property(e => e.OrderId)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RecentUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StaffCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<HospitalMapping>(entity =>
            {
                entity.ToTable("HospitalMapping");

                entity.Property(e => e.HospitalCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.HospitalName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InsertDate).HasColumnType("datetime");

                entity.Property(e => e.OrderId)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProviderGuid)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RecentUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.StaffCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.HasOne(d => d.ProviderGu)
                    .WithMany(p => p.HospitalMappings)
                    .HasForeignKey(d => d.ProviderGuid)
                    .HasConstraintName("FK_HospitalMapping_Provider");
            });

            modelBuilder.Entity<Language>(entity =>
            {
                entity.Property(e => e.Code)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InsertDate).HasColumnType("datetime");

                entity.Property(e => e.RecentUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Text)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<LanguageMapping>(entity =>
            {
                entity.ToTable("LanguageMapping");

                entity.Property(e => e.InsertDate).HasColumnType("datetime");

                entity.Property(e => e.LanguageCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProviderGuid)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RecentUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Text)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.HasOne(d => d.ProviderGu)
                    .WithMany(p => p.LanguageMappings)
                    .HasForeignKey(d => d.ProviderGuid)
                    .HasConstraintName("FK_LanguageMapping_Provider");
            });

            modelBuilder.Entity<LicenseMapping>(entity =>
            {
                entity.ToTable("LicenseMapping");

                entity.Property(e => e.AwardDate)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ContactEmail)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("Contact_Email");

                entity.Property(e => e.ContactFax)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("Contact_Fax");

                entity.Property(e => e.ContactName)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("Contact_Name");

                entity.Property(e => e.ContactPhone)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("Contact_Phone");

                entity.Property(e => e.ExpirationDate)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InsertDate).HasColumnType("datetime");

                entity.Property(e => e.InstitutionAddress1)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("Institution_Address1");

                entity.Property(e => e.InstitutionAddress2)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("Institution_Address2");

                entity.Property(e => e.InstitutionCity)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("Institution_City");

                entity.Property(e => e.InstitutionContact)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("Institution_Contact");

                entity.Property(e => e.InstitutionName)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("Institution_Name");

                entity.Property(e => e.InstitutionState)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("Institution_State");

                entity.Property(e => e.InstitutionZip)
                    .HasMaxLength(100)
                    .IsUnicode(false)
                    .HasColumnName("Institution_Zip");

                entity.Property(e => e.LicenseNumber)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LicenseStatus)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LicenseType)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LicensureField)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProviderGuid)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RecentUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.State)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.HasOne(d => d.ProviderGu)
                    .WithMany(p => p.LicenseMappings)
                    .HasForeignKey(d => d.ProviderGuid)
                    .HasConstraintName("FK_LicenseMapping_Provider");
            });

            modelBuilder.Entity<Location>(entity =>
            {
                entity.HasKey(e => e.LocationGuid);

                entity.Property(e => e.LocationGuid)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Address1).IsUnicode(false);

                entity.Property(e => e.Address2).IsUnicode(false);

                entity.Property(e => e.Address3).IsUnicode(false);

                entity.Property(e => e.AdmissionsPhone)
                    .IsUnicode(false)
                    .HasColumnName("admissionsPhone");

                entity.Property(e => e.City).IsUnicode(false);

                entity.Property(e => e.Fax).IsUnicode(false);

                entity.Property(e => e.Id).ValueGeneratedOnAdd();

                entity.Property(e => e.InsertDate).HasColumnType("datetime");

                entity.Property(e => e.IsAcceptingEappointments).HasColumnName("IsAcceptingEAppointments");

                entity.Property(e => e.MainPhone)
                    .IsUnicode(false)
                    .HasColumnName("mainPhone");

                entity.Property(e => e.Name).IsUnicode(false);

                entity.Property(e => e.Phone).IsUnicode(false);

                entity.Property(e => e.RecentUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.State).IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.Zip).IsUnicode(false);
            });

            modelBuilder.Entity<LocationMapping>(entity =>
            {
                entity.ToTable("LocationMapping");

                entity.Property(e => e.LocationGuid)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ProviderGuid)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Type).IsUnicode(false);

                entity.HasOne(d => d.LocationGu)
                    .WithMany(p => p.LocationMappings)
                    .HasForeignKey(d => d.LocationGuid)
                    .HasConstraintName("FK_LocationMapping_LocationMapping");

                entity.HasOne(d => d.ProviderGu)
                    .WithMany(p => p.LocationMappings)
                    .HasForeignKey(d => d.ProviderGuid)
                    .HasConstraintName("FK_LocationMapping_Provider");
            });

            modelBuilder.Entity<LocationsHour>(entity =>
            {
                entity.ToTable("Locations_Hours");

                entity.Property(e => e.CloseTime)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.DayOfWeek)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.InsertDate).HasColumnType("datetime");

                entity.Property(e => e.LocationGuid)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.OpenTime)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.RecentUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.HasOne(d => d.LocationGu)
                    .WithMany(p => p.LocationsHours)
                    .HasForeignKey(d => d.LocationGuid)
                    .HasConstraintName("FK_Locations_Hours_Locations");
            });

            modelBuilder.Entity<LocationsOfficeExtender>(entity =>
            {
                entity.ToTable("Locations_OfficeExtenders");

                entity.Property(e => e.CommonName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ExtenderGuid)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InsertDate).HasColumnType("datetime");

                entity.Property(e => e.JobTitle)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LocationGuid)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RecentUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UrlRoute).IsUnicode(false);

                entity.HasOne(d => d.LocationGu)
                    .WithMany(p => p.LocationsOfficeExtenders)
                    .HasForeignKey(d => d.LocationGuid)
                    .HasConstraintName("FK_Locations_OfficeExtenders_Locations");
            });

            modelBuilder.Entity<LocationsProvider>(entity =>
            {
                entity.ToTable("Locations_Providers");

                entity.Property(e => e.CommonName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InsertDate).HasColumnType("datetime");

                entity.Property(e => e.JobTitle)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.LocationGuid)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ProviderGuid)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RecentUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UrlRoute).IsUnicode(false);

                entity.HasOne(d => d.LocationGu)
                    .WithMany(p => p.LocationsProviders)
                    .HasForeignKey(d => d.LocationGuid)
                    .HasConstraintName("FK_Locations_Providers_Locations");
            });

            modelBuilder.Entity<PatientFormsMapping>(entity =>
            {
                entity.ToTable("PatientFormsMapping");

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.InsertDate).HasColumnType("datetime");

                entity.Property(e => e.Link)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.Name)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.ProviderGuid)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RecentUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Type)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.HasOne(d => d.ProviderGu)
                    .WithMany(p => p.PatientFormsMappings)
                    .HasForeignKey(d => d.ProviderGuid)
                    .HasConstraintName("FK_PatientFormsMapping_Provider");
            });

            modelBuilder.Entity<Provider>(entity =>
            {
                entity.HasKey(e => e.Guid);

                entity.ToTable("Provider");

                entity.Property(e => e.Guid)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ContactCellPhone)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ContactPager)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.ContactPhone)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.DoctorHeaderImage)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.DoctorImage)
                    .HasMaxLength(500)
                    .IsUnicode(false);

                entity.Property(e => e.EchoDoctorNumber)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EchoPhysicianId)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EchoSuffix)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FirstName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FormattedCommonName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FormattedJobTitle)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FormattedLastNameFirst)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.FormattedSpecialties).HasColumnType("text");

                entity.Property(e => e.FormattedUrlRoute)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.GenderId)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.InsertDate).HasColumnType("datetime");

                entity.Property(e => e.Introduction).HasColumnType("text");

                entity.Property(e => e.IsBpp).HasColumnName("IsBPP");

                entity.Property(e => e.IsPcmh).HasColumnName("IsPCMH");

                entity.Property(e => e.JobTitleOverride)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LastName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LastUpdateDateTime)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LastUpdateUser)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.LegalPracticeName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.MiddleName)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NameOverride)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.NationalId)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.PublicationQuery).HasColumnType("text");

                entity.Property(e => e.RecentUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.ThumbnailPhoto)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.Property(e => e.UrlRoute)
                    .HasMaxLength(255)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<ProviderParagraph>(entity =>
            {
                entity.Property(e => e.InsertDate).HasColumnType("datetime");

                entity.Property(e => e.OrderId)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProviderGuid)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RecentUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Text).IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.HasOne(d => d.ProviderGu)
                    .WithMany(p => p.ProviderParagraphs)
                    .HasForeignKey(d => d.ProviderGuid)
                    .HasConstraintName("FK_ProviderParagraphs_Provider");
            });

            modelBuilder.Entity<Specialty>(entity =>
            {
                entity.Property(e => e.BoardCertification)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.BoardDescription)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.CertifyingBoardName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Code)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.DropDownValue)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InsertDate).HasColumnType("datetime");

                entity.Property(e => e.JobTitle)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.RecentUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Text)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");
            });

            modelBuilder.Entity<SpecialtyMapping>(entity =>
            {
                entity.ToTable("SpecialtyMapping");

                entity.Property(e => e.HippaTaxonomy)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InsertDate).HasColumnType("datetime");

                entity.Property(e => e.JobTitle)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.ProviderGuid)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RecentUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SpecialtyCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SpecialtyStatus)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.SpecialtyType)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.Text)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.HasOne(d => d.ProviderGu)
                    .WithMany(p => p.SpecialtyMappings)
                    .HasForeignKey(d => d.ProviderGuid)
                    .HasConstraintName("FK_SpecialtyMapping_Provider");
            });

            modelBuilder.Entity<SuffixMapping>(entity =>
            {
                entity.ToTable("SuffixMapping");

                entity.Property(e => e.InsertDate).HasColumnType("datetime");

                entity.Property(e => e.ProviderGuid)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RecentUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.SuffixCode)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.HasOne(d => d.ProviderGu)
                    .WithMany(p => p.SuffixMappings)
                    .HasForeignKey(d => d.ProviderGuid)
                    .HasConstraintName("FK_SuffixMapping_Provider");
            });

            modelBuilder.Entity<Type>(entity =>
            {
                entity.HasKey(e => e.IdKey);

                entity.ToTable("Type");

                entity.Property(e => e.Code)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.InsertDate).HasColumnType("datetime");

                entity.Property(e => e.ProviderGuid)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.RecentUser)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.Text)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.UpdateDate).HasColumnType("datetime");

                entity.HasOne(d => d.ProviderGu)
                    .WithMany(p => p.Types)
                    .HasForeignKey(d => d.ProviderGuid)
                    .HasConstraintName("FK_Type_Provider");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
