﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PhysiciansMicroservice.Models
{
    public partial class Provider
    {
        public Provider()
        {
            AgesTreatedMappings = new HashSet<AgesTreatedMapping>();
            BoardMappings = new HashSet<BoardMapping>();
            EducationMappings = new HashSet<EducationMapping>();
            Expertises = new HashSet<Expertise>();
            GroupsMappings = new HashSet<GroupsMapping>();
            HospitalMappings = new HashSet<HospitalMapping>();
            LanguageMappings = new HashSet<LanguageMapping>();
            LicenseMappings = new HashSet<LicenseMapping>();
            LocationMappings = new HashSet<LocationMapping>();
            PatientFormsMappings = new HashSet<PatientFormsMapping>();
            ProviderParagraphs = new HashSet<ProviderParagraph>();
            SpecialtyMappings = new HashSet<SpecialtyMapping>();
            SuffixMappings = new HashSet<SuffixMapping>();
            Types = new HashSet<Type>();
        }

        public int Id { get; set; }
        public string Guid { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string FormattedCommonName { get; set; }
        public string FormattedLastNameFirst { get; set; }
        public string EchoPhysicianId { get; set; }
        public string EchoDoctorNumber { get; set; }
        public string NationalId { get; set; }
        public string ContactPhone { get; set; }
        public string ContactCellPhone { get; set; }
        public string ContactPager { get; set; }
        public string GenderId { get; set; }
        public string DoctorImage { get; set; }
        public string DoctorHeaderImage { get; set; }
        public string ThumbnailPhoto { get; set; }
        public string EchoSuffix { get; set; }
        public string UrlRoute { get; set; }
        public string FormattedUrlRoute { get; set; }
        public bool? IsAcceptingNewPatients { get; set; }
        public bool? IsAcceptingNewPatientsVisible { get; set; }
        public int? TypeId { get; set; }
        public string Introduction { get; set; }
        public string FormattedSpecialties { get; set; }
        public bool? IsArchived { get; set; }
        public bool? IsHide { get; set; }
        public string NameOverride { get; set; }
        public string JobTitleOverride { get; set; }
        public string FormattedJobTitle { get; set; }
        public string LegalPracticeName { get; set; }
        public bool? DoesAcceptsEapps { get; set; }
        public bool? IsPcmh { get; set; }
        public bool? IsBloodless { get; set; }
        public bool? AdminApproved { get; set; }
        public bool? IsNewDoctor { get; set; }
        public bool? IsBpp { get; set; }
        public bool IsEmployed { get; set; }
        public bool? DoesHavePatientReviews { get; set; }
        public string PublicationQuery { get; set; }
        public string LastUpdateDateTime { get; set; }
        public string LastUpdateUser { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime InsertDate { get; set; }
        public string RecentUser { get; set; }

        public virtual ICollection<AgesTreatedMapping> AgesTreatedMappings { get; set; }
        public virtual ICollection<BoardMapping> BoardMappings { get; set; }
        public virtual ICollection<EducationMapping> EducationMappings { get; set; }
        public virtual ICollection<Expertise> Expertises { get; set; }
        public virtual ICollection<GroupsMapping> GroupsMappings { get; set; }
        public virtual ICollection<HospitalMapping> HospitalMappings { get; set; }
        public virtual ICollection<LanguageMapping> LanguageMappings { get; set; }
        public virtual ICollection<LicenseMapping> LicenseMappings { get; set; }
        public virtual ICollection<LocationMapping> LocationMappings { get; set; }
        public virtual ICollection<PatientFormsMapping> PatientFormsMappings { get; set; }
        public virtual ICollection<ProviderParagraph> ProviderParagraphs { get; set; }
        public virtual ICollection<SpecialtyMapping> SpecialtyMappings { get; set; }
        public virtual ICollection<SuffixMapping> SuffixMappings { get; set; }
        public virtual ICollection<Type> Types { get; set; }
    }
}
