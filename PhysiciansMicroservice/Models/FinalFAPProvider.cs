﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhysiciansMicroservice.Models
{
    public class FinalFAPProvider
    {
        public FapprovidersView _Fapproviders { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Address3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Fax { get; set; }
        public double? Lat { get; set; }
        public double? Lng { get; set; }
        public string MainPhone { get; set; }
    }

}
