﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PhysiciansMicroservice.Models
{
    public partial class LocationsProvider
    {
        public int Id { get; set; }
        public string LocationGuid { get; set; }
        public string ProviderGuid { get; set; }
        public string CommonName { get; set; }
        public string UrlRoute { get; set; }
        public string JobTitle { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime InsertDate { get; set; }
        public string RecentUser { get; set; }

        public virtual Location LocationGu { get; set; }
    }
}
