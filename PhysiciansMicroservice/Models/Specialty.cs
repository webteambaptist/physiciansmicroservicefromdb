﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PhysiciansMicroservice.Models
{
    public partial class Specialty
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string Text { get; set; }
        public string DropDownValue { get; set; }
        public string JobTitle { get; set; }
        public string CertifyingBoardName { get; set; }
        public string BoardCertification { get; set; }
        public string BoardDescription { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime InsertDate { get; set; }
        public string RecentUser { get; set; }
    }
}
