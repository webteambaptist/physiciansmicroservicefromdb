﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PhysiciansMicroservice.Models
{
    public class ProviderPcdbImport
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string EchoDoctorNumber { get; set; }
    }
}
