﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PhysiciansMicroservice.Models
{
    public partial class GroupsMapping
    {
        public int Id { get; set; }
        public string ProviderGuid { get; set; }
        public string Code { get; set; }
        public string Text { get; set; }
        public string Type { get; set; }
        public int? OrderId { get; set; }
        public bool? IsPrimary { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime InsertDate { get; set; }
        public string RecentUser { get; set; }

        public virtual Provider ProviderGu { get; set; }
    }
}
