﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PhysiciansMicroservice.Models
{
    public partial class LicenseMapping
    {
        public int Id { get; set; }
        public string ProviderGuid { get; set; }
        public string LicenseType { get; set; }
        public string LicenseStatus { get; set; }
        public string State { get; set; }
        public string LicenseNumber { get; set; }
        public string AwardDate { get; set; }
        public string ExpirationDate { get; set; }
        public string ContactName { get; set; }
        public string ContactPhone { get; set; }
        public string ContactFax { get; set; }
        public string ContactEmail { get; set; }
        public string LicensureField { get; set; }
        public string InstitutionName { get; set; }
        public string InstitutionContact { get; set; }
        public string InstitutionAddress1 { get; set; }
        public string InstitutionAddress2 { get; set; }
        public string InstitutionCity { get; set; }
        public string InstitutionState { get; set; }
        public string InstitutionZip { get; set; }
        public bool? Active { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime InsertDate { get; set; }
        public string RecentUser { get; set; }

        public virtual Provider ProviderGu { get; set; }
    }
}
