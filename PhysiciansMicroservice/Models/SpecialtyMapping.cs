﻿using System;
using System.Collections.Generic;

#nullable disable

namespace PhysiciansMicroservice.Models
{
    public partial class SpecialtyMapping
    {
        public int Id { get; set; }
        public string ProviderGuid { get; set; }
        public string SpecialtyCode { get; set; }
        public string Text { get; set; }
        public string JobTitle { get; set; }
        public string SpecialtyStatus { get; set; }
        public string SpecialtyType { get; set; }
        public string HippaTaxonomy { get; set; }
        public bool? IsBoardCertified { get; set; }
        public bool? IsPrimary { get; set; }
        public DateTime? UpdateDate { get; set; }
        public DateTime InsertDate { get; set; }
        public string RecentUser { get; set; }

        public virtual Provider ProviderGu { get; set; }
    }
}
